---
marp: true
theme: ame
size: 4K
paginate: true
footer: '[![](/images/Antoine_mini.png)](https://github.com/AntoineMeheut)[Antoine Meheut](https://github.com/AntoineMeheut)'
header: '[&#9635;](#1, " ")  &nbsp; Futur of IDE'
---

<!-- class: invert -->
# Futur of IDE
### How to reduce development constraints
Paris, October 2022
Ajoute

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/cloud_ide/FutureIDE)
[![Slides](https://cdn-icons-png.flaticon.com/128/609/609001.png)](https://demo-gitpod.gitlab.io/FutureIDE/README.html)

---

## Development environment (IDE)
* In computer programming, a development environment is a set of tools that can increase the productivity of programmers who develop software,
* It includes a text editor intended for programming, functions which allow, by pressing a button, to start the compiler or the linker as well as an online debugger, which allows to execute line by line the program under construction,
* The next step is to use a complete software factory to improve the quality and security of developments,
* The next next step is in this presentation ...

---

## Back to the Futur : Access to IDEs over time (1/2)
| Year | Terminal | Photo |
|:-:|:-:|:-:|
| 1890 | Herman Hollerith's Punch Card | ![drop-shadow:0,5px,10px,rgba(0,0,0,.4) w:200 h:100](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Punched_card.jpg/340px-Punched_card.jpg) |
| 1959 | The IBM 1620 terminal | ![drop-shadow:0,5px,10px,rgba(0,0,0,.4) w:200 h:150](https://upload.wikimedia.org/wikipedia/commons/e/e9/IBM_1620_Model_1.jpg) |
| 1976 | The Apple I, birth of personal computers | ![drop-shadow:0,5px,10px,rgba(0,0,0,.4) w:200 h:150](https://www.old-computers.com/museum/photos/apple_1.jpg) |

---

## Back to the Futur : Access to IDEs over time (1/2)
| Year | Terminal | Photo |
|:-:|:-:|:-:|
| 1981 | The first IBM PC with 8-inch floppy disks| ![drop-shadow:0,5px,10px,rgba(0,0,0,.4) w:200 h:150](https://upload.wikimedia.org/wikipedia/commons/1/12/IBM_5120_Computer_System.jpg) |
| 1999 | Creation of VMware and virtual machines| ![drop-shadow:0,5px,10px,rgba(0,0,0,.4) w:200 h:100](https://www.trucnet.com/wp-content/uploads/2020/05/VMware-Workstation-Pro-16.jpg) |
| 2010 | The cloud, kubernetes and the IDE of the future| ![drop-shadow:0,5px,10px,rgba(0,0,0,.4) w:200 h:150](https://venturebeat.com/wp-content/uploads/2021/04/feat-2-e1617697832604.png?fit=400%2C200&strip=all) |

---

## My developer needs today
- Do not depend on the power of the computer I work on (computers at 4000 €)
- Don't waste time installing my development environment
- Have my software factory in a single (Git) entry point
- Have in my IDE the technical architecture target
- Deliver my code directly in the integration environment or even production
- Demonstrate my code even if the target architecture is not ready

---

## Some solutions available today

- [Gitpod](https://gitpod.io)
- [OpenShift Dev Spaces](https://developers.redhat.com/products/openshift-dev-spaces/overview)
- [Github Codespace](https://github.com/features/codespaces)
- [Scalingo - Outscale](https://scalingo.com)
